# Flagship Interview Laravel Code Test
This is a code test built using the Laravel PHP framework that will test your knowledge of arrays, sorting, and consuming data from the frontend.

# Installation

Follow the installation instructions provided by Laravel version 5.4 from the Laravel website:
https://laravel.com/docs/5.4/installation

You should only need composer as the Laravel project has already been created for you.

Once Composer is installed on your system:
```
cd src/
php composer.phar install # alternatively, run composer install if global
```

If you receive errors related to ciphers not being supported, regenerate your application key:
```
php artisan config:clear
php artisan key:generate
```

# Run
```
cd src/
php artisan serve
```

# Challenge
We are asking you to complete backend and frontend sections of this document.

## Backend
The FlagshipController contains showUser and showUsers methods that are incomplete implementations.

The /api/users API endpoint route has already been wired up, however we would like a new endpoint route for retrieving a single user based on the ID passed in from the request which you will need to create. For example, requesting /api/users/1 should return a single user with the ID of 1.

Comments have been provided in the FlagshipController PHP class that is located in the project.

## Frontend
Make an AJAX request to the /api/users endpoint and show the list of users on the homepage underneath the banner on page load.

# Completion
When you are complete, provide us with a link to the repository that can be accessed via git clone or http download (a ZIP archive is acceptable).

Good luck!
