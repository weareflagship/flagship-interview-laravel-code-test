<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use View;

class FlagshipController extends BaseController
{
	// User list, do not change
	// Ideally this would be pulled dynamically from a database but we have stored it here for complexity reasons
	private $users = [
		'0' => [
			'id' => 1,
			'username' => 'jack.sparrow',
			'password' => '5f4dcc3b5aa765d61d8327deb882cf99' // MD5 "password"
		],
		'1' => [
			'id' => 2,
			'username' => 'davey.jones',
			'password' => '7c6a180b36896a0a8c02787eeafb0e4c' // MD5 "password1"
		],
		'2' => [
			'id' => 3,
			'username' => 'elizabeth.swann',
			'password' => '6cb75f652a9b52798eb6cf2201057c73' // MD5 "password2"
		],
		'3' => [
			'id' => 4,
			'username' => 'tom.jones',
			'password' => '6cb75f652a9b52798eb6cf2201057c73' // MD5 "password2"
		]
	];

    public function showIndex()
    {
    	return View::make('index');
    }

    public function showUser()
    {
    	/*
    	 * TODO:
    	 * 1. Return a single user by ID from the request
    	 *
    	 */
    }

    public function showUsers()
    {
    	/*
    	 * TODO:
    	 * 1. Remove passwords from the users array before returning result
    	 * 2. Sort result array alphanumerically by username
    	 */
    	return response()->json($this->users);
    }
}
